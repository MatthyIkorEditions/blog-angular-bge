import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input() postItems;

  constructor() { }

  ngOnInit() {
  }

  displayPostItems() {
      var text = '';
      for(var i = 0; i < this.postItems.length; i++) {
          text += this.postItems[i].title + ' - ' + this.postItems[i].content;
      }
      return text;
  }

}
