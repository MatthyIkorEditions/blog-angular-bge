import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input() itemTitle: string;
  @Input() itemContent: string;
  @Input() itemDate: object;
  @Input() itemLike: number;
  @Input() itemDislike: number;

  constructor() { }

  ngOnInit() {
  }

  onLike() {
      this.itemLike++;
  }

  onDislike() {
      this.itemDislike++;
  }
}
